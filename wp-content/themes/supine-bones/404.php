<?php get_header(); ?>

	<div id="container">
		
<div id="page" class="group">
	<div id="page-content" class="group">
		<div id="content" class="group">
			<div id="main" class="eightcol first clearfix" role="main">

				<article id="post-not-found" class="clearfix">
					
					<img class="susannah-404" src="<?php echo get_template_directory_uri(); ?>/library/images/susannah-headshot_568x270.jpg" alt="Headshot of Susannah Pyne" />

					<header class="article-header">

						<h1>Sorry&hellip;</h1>
						<h1>&nbsp;&nbsp;&hellip;but I can't find the article you're looking for!</h1>

					</header>

					<section class="entry-content">
						
						<p>You can use the Menu bar above to choose another page or, if you are looking for something specific, you can use the Search box below.</p>
						
					</section>
					
					<section class="search-404">

						<p>
							<form role="search" method="get" id="searchform" action="http://supine.susandjay.com/" >
								<input type="text" value="" name="s" id="s" />
								<input type="submit" id="searchsubmit" value="Search Now" />
							</form>
						</p>

					</section>

				</article>

			</div>
		</div>

<?php get_sidebar(); ?>
			
	</div>
</div>

	</div> <!-- end #container -->

<?php get_footer(); ?>
