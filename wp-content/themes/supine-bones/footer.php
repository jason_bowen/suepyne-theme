
<div id="footer">
	<div id="footer-content" class="group">
			
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1') ) : ?>
<?php endif; ?>
			
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2') ) : ?>
<?php endif; ?>

<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3') ) : ?>
<?php endif; ?>

		<div style="clear: both"></div>
		<div class="copyright">
			<p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
		</div>
	
	</div>

</div>


<!--			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">

					<nav role="navigation">
							<?php /* bones_footer_links(); */ ?>
					</nav>
-->
					

				<!-- </div> --> <!-- end #inner-footer -->

			<!-- </footer> --> <!-- end footer -->

<!-- </div> --> <!-- end #container -->

		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

	</body>

</html> <!-- end page. what a ride! -->
