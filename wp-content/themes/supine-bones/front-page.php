<?php
/*
Template Name: Front Page
*/
?>

<?php get_header(); ?>

	<div id="front-page-banner">
		<div id="front-page-banner-content">
			<div id="slider-container"><img src="/wp-content/uploads/2013/09/supine_main_image_2.jpg" width="580" height="360" alt="Susannah Pyne - Owner of Supine Fitness" /></div>
			<div id="newsletter-container">
				<h2>Sign Up For My FREE<br />5 Day Princess Program NOW!</h2>
				<p>First name:<br /><input type="text" id="firstname" /></p>
				<p>Email address:<br /><input type="text" id="email-address" /></p>
				<input type="submit" name="email-submit" value="Yes! Let's Get Started!" />
				<div id="privacy-statement">
					<p>We don&apos;t sell or give away your email address!<br /><a href="#">Privacy policy</a></p>
				</div>
			</div>
		</div>
	</div>

	<div id="container">
		
<div id="page" class="group">
		<div id="page-content" class="group">
				<div id="content" class="group">
						<div id="main" class="front-page eightcol first clearfix" role="main">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

<?php
		$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'thumbnail') );
		if ($url != '') {
?>		
				<div class="article-featured-image clearfix">
						<img src="<?php echo $url?>" />
				</div>
<?php
		}

?>

<!-- HEADING COMMENTED OUT 
				<header class="article-header">
					<h1 class="h2">
						<a href="<?php /* the_permalink() */ ?>" rel="bookmark" title="<?php /* the_title_attribute(); */ ?>"><?php /* the_title(); */ ?></a>
					</h1>
				</header>
-->

				<section class="entry-content clearfix">

<?php the_content(__('Now read the rest!'));?>

										</section>
										<footer class="article-footer">
												<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'bonestheme') . '</span> ', ', ', ''); ?></p>
										</footer>

<?php // comments_template(); // uncomment if you want to use them ?>

								</article> <!-- end article -->

<?php endwhile; ?>

<?php
		if (function_exists('bones_page_navi')) {	bones_page_navi();
		} else {
?>

								<nav class="wp-prev-next">
										<ul class="clearfix">
												<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
												<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
										</ul>
								</nav>
<?php	}	else :	?>

								<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
												<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
										</header>
										<section class="entry-content">
												<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e("This is the error message in the index.php template.", "bonestheme"); ?></p>										</footer>
								</article>

<?php	endif;	?>


<!-- DISPLAY LATEST POST -->

<?php
$posts = get_posts('numberposts=1');
foreach ($posts as $post):
start_wp();
the_post();
endforeach;
?>

						</div>
				</div>

<?php get_sidebar(); ?>
			
	</div>
</div>

	</div> <!-- end #container -->

<?php get_footer(); ?>