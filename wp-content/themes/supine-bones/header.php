<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">
		<meta name="description" content="Supine Fitness will help you accelerate your weight loss and fitness goals. Talk to us to find out more!" />
		<meta name="author" content="Susannah Pine - Supine Fitness" />

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<title><?php wp_title(''); ?> | Supine Fitness</title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True" />
		<meta name="MobileOptimized" content="320" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png" />
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/library/images/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/library/images/favicon.ico" type="image/x-icon" />
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#A7228A" />
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png" />

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- drop Google Analytics Here -->
		<!-- end analytics -->
	
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/supine.css" />
		<!--[if gte IE 9]>
			<style type="text/css">
				.gradient {
					filter: none;
				}
			</style>
		<![endif]-->

	</head>

	<body <?php body_class(); ?>>

	<div id="header">
		<div id="header-content">
			<a href="<?php echo home_url(); ?>" rel="nofollow"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/library/images/supine-fitness-logo_322x80.jpg" alt="Supine Fitness Logo" width="322" height="80" /></a>
			<div id="header-phone">07899 074287</div>
			<div id="social-media">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/social-media-icons_224x32.jpg" width="224" height="32" alt="Social Media icons - Facebook, LinkedIn and Twitter" usemap="#socialmedia" />
				<map id="socialmedia" name="socialmedia">
				  <area shape="rect" coords="1,1,30,30" href="https://www.facebook.com/SupineFitness" target="_blank" alt="Like us on Facebook" />
				  <area shape="rect" coords="65,1,94,30" href="http://uk.linkedin.com/pub/sue-pyne/76/3aa/92b/" target="_blank" alt="Network with us on LinkedIn" />
				  <area shape="rect" coords="129,1,158,30" href="skype:spflady?call" alt="Chat with me!" />
				  <area shape="rect" coords="193,1,222,30" href="https://twitter.com/supinefitness" target="_blank" alt="Follow us on Twitter" />
				</map>
			</div>
		</div>
	</div>
	<div id="main-menu">
		<?php wp_nav_menu(); ?>
	</div>


	