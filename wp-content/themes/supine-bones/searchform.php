<div id="supine-search" class="search-box widget widget_search">
	<h4 class="widgettitle">Search</h4>
	<form method="get" id="searchform" action="<?php echo home_url() ; ?>/">
		<input type="text" value="<?php echo esc_html($s, 1); ?>" name="s" id="s" maxlength="33" />
		<div id="searchbox-submit" class="clearfix">
			<input type="submit" class="button" value="Search Now"/>
		</div>
	</form>
</div>