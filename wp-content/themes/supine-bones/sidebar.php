<div id="sidebar1" class="sidebar fourcol last clearfix" role="complementary">
	
	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>
		
		<?php dynamic_sidebar( 'sidebar1' ); ?>
		
	<?php else : ?>
		
		<!-- This content shows up if there are no widgets defined in the backend. -->
		<div class="alert alert-help">
			<p><?php _e("Please activate some Widgets.", "bonestheme");  ?></p>
		</div>
		
	<?php endif; ?>

	<?php /* include('searchform.php'); */ ?>
	
	<div id="facebook" class="widget widget_search">
		<h4 class="widgettitle">Facebook</h4>
		<iframe style="border: none; overflow: hidden; width: 298px; height: 480px;" src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FSupineFitness&amp;width=298&amp;height=480&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true&amp;appId=306246479417818" height="298" width="298"></iframe>
	</div>
	
</div>